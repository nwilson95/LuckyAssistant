import speech_recognition as sr
import gtts
from playsound import playsound
from pynytimes import NYTAPI
import webbrowser
import os

# Language choice
# en{'com', 'com.au', 'co.uk', 'com', 'ca', 'co.in', 'ie', 'co.za'}
# fr{'ca', 'fr'}
# zh-CN{any}
# zh-TW{any}
# pt{'com.br', 'pt'}
# es{'com.mx', 'es', 'com'}
LANG = 'en'
TOPLEVEL = 'com'

KEY = {NYTIMES API KEY}
nyt = NYTAPI(KEY, parse_dates=True)

def startLucky():
    while (True):
        r = sr.Recognizer()
        with sr.Microphone() as source:
            audio = r.listen(source)

        command = r.recognize_google(audio)

        # Basic tts structure
        # tts = gtts.gTTS("Text to run TTL on")
        # tts.save("locationToSave.mp3")

        # Handling commands, need to find a better way to detect what is wanted. Seems to misunderstand words a lot.
        # Hot word check
        if any(word in str(command).lower().split(' ') for word in {'hey', 'goodbye'}) and \
                any(word in str(command).lower().split(' ') for word in {'lucky'}):
            # Check to see if assistant is online.
            if any(word in str(command).lower().split(' ') for word in {'are', 'you', 'alive'}):
                print("Health check successful.")
                playsound("./audioAssets/health check.mp3")
            # Gets weather for today.
            elif any(word in str(command).lower().split(' ') for word in {'weather', 'today'}) and \
                    not any(word in str(command).lower().split(' ') for word in {'tomorrow'}):
                playsound("./audioAssets/incomplete.mp3")
            # Gets weather for tomorrow
            elif any(word in str(command).lower().split(' ') for word in {'weather', 'tomorrow'}) and \
                    not any(word in str(command).lower().split(' ') for word in {'today'}):
                playsound("./audioAssets/incomplete.mp3")
            # Shuts lucky down
            elif 'goodbye' in str(command).lower().split(' '):
                playsound("./audioAssets/goodbye.mp3")
                break
            # Works with NYT API to get article info, and opens link to site if wanted.
            elif any(word in str(command).lower().split(' ') for word in {'new', 'york', 'times', 'top', 'stories'}):
                playsound("./audioAssets/nytimes.mp3")
                r = sr.Recognizer()
                with sr.Microphone() as source:
                    command = r.listen(source)
                    results = nyt.top_stories(section=str(r.recognize_google(command)))
                    articleDict = dict()

                    trueIndex = 1
                    for x in range(0, 3, 1):
                        title = results[x]['title']
                        if title == '' or any({'sign', 'up'} in str(title).lower().split(' ')):
                            continue
                        articleDict[trueIndex] = (results[x]['url'])
                        gtts.gTTS(f'Article {trueIndex} {title}').save("./tempAssets/nyt.mp3")
                        playsound('./tempAssets/nyt.mp3')
                        trueIndex += 1

                gtts.gTTS("What article number would you like to read?").save("./tempAssets/nyt.mp3")
                playsound("./tempAssets/nyt.mp3")
                r = sr.Recognizer()
                with sr.Microphone() as source:
                    command = r.listen(source)
                    result = r.recognize_google(command)
                    if result != 'no':
                        webbrowser.open_new_tab(articleDict[int(result)])
                        os.remove("./tempAssets/nyt.mp3")

            else:
                print(f'Command is invalid: {str(command)}')
                tts = gtts.gTTS("I did not understand your request, please try again.")
                tts.save("./audioAssets/invalid.mp3")
                playsound("./audioAssets/invalid.mp3")
        else:
            print(f'Words heard, but not recognized: {str(command)}')
            continue

if __name__ == '__main__':
    startLucky()
