# LuckyAssistant
LuckyAssistant is an assistant that I am creating in my free time to do whatever I want it to do. <br/>
I started this project just as a way to dive into more things python related. <br/>
I am also working on learning some more about machine learning and making models the end goal is to start including these in this project once I have a solid grasp.<br/>

## Current to do
- Integrate a weather API
- Move over any configurable values to a config file
- Make the hot word and name apart of the config file

## Things I am looking into
- Add the ability to launch applications
- Integrate ML Models foradding book and movie recommendation functionality
- Add in better voice recognition, make it where Lucky takes what you say, and run against a test to find what it thinks you said
- Look at moving away from gTTS and speech recognition packages for offline support and not being dependant on google
